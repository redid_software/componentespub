unit DBLookupComboBoxMC;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, DbCtrls,
  Buttons, StdCtrls, messages, LCLType;

type
  TDBLookupComboBoxMC = class(TDBLookupComboBox)
  private
    FEdit        : TEdit;
    CorFEntrada  : TColor;
    CorFSaida    : TColor;
    CorTEntrada  : TColor;
    CorTSaida    : TColor;

    procedure SetFMudaCor(Value : TColor);
    procedure SetFVoltaCor(Value : TColor);
    procedure SetTMudaCor(Value : TColor);
    procedure SetTVoltaCor(Value : TColor);
  protected
    procedure CreateParams(var Params : TCreateParams); override;
    procedure DoEnter; override;
    procedure DoExit; override;
    procedure DoClick(Sender : TObject);
  public
    procedure DropDown; override;
    procedure CloseUp; override;//dynamic;
    constructor Create(Sender : TComponent); override;
  published
    property Edit            : TEdit        read FEdit        write FEdit;
    property CorFundoEntrada : TColor       read CorFEntrada write SetFMudaCor;
    property CorTextoEntrada : TColor       read CorTEntrada write SetTMudaCor;
    property CorFundoSaida   : TColor       read CorFSaida   write SetFVoltaCor;
    property CorTextoSaida   : TColor       read CorTSaida   write SetTVoltaCor;
  end;

procedure Register;

implementation

//Inicio -> Set Cor Fundo
procedure TDBLookupComboBoxMC.SetFMudaCor(Value : TColor);
begin
  CorFEntrada := Value;
end;

procedure TDBLookupComboBoxMC.SetFVoltaCor(Value : TColor);
begin
  CorFSaida := Value;
end;
//Fim -> Set Cor Texto

//Inicio -> Set Cor Texto
procedure TDBLookupComboBoxMC.SetTMudaCor(Value : TColor);
begin
  CorTEntrada := Value;
end;

procedure TDBLookupComboBoxMC.SetTVoltaCor(Value : TColor);
begin
  CorTSaida := Value;
end;

procedure TDBLookupComboBoxMC.CloseUp;
begin
  inherited CloseUp;
end;

procedure TDBLookupComboBoxMC.DropDown;
begin
  TDBLookupComboBoxMC(Self).Color := TDBLookupComboBoxMC(Self).Color;
  inherited DropDown;
end;

//Fim -> Set Cor Texto
procedure TDBLookupComboBoxMC.DoClick(Sender : TObject);
begin
  inherited;
  if trim(Text) <> '' then
    SelectAll;
end;

procedure TDBLookupComboBoxMC.DoEnter;
begin
  Color := CorFEntrada;
  Font.Color := CorTEntrada;
  inherited DoEnter;
end;

procedure TDBLookupComboBoxMC.DoExit;
begin
  Color := CorFSaida;
  Font.Color := CorTSaida;
  inherited DoExit;
end;

constructor TDBLookupComboBoxMC.Create(Sender : TComponent);
begin
  inherited Create(Sender);
  OnClick := @DoClick;
end;

procedure TDBLookupComboBoxMC.CreateParams(var Params : TCreateParams);
begin
  inherited CreateParams(Params);
  Params.Style := Params.Style or WS_CLIPCHILDREN;
end;



procedure Register;
begin
  {$I icones.lrs}
  RegisterComponents('RedID',[TDBLookupComboBoxMC]);
end;

end.
