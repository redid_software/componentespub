unit DBMemoMC;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, DbCtrls, db;

type
  TDBMemoMC = class(TDBMemo)
  private
    CorFEntrada : TColor;
    CorFSaida : TColor;
    CorTEntrada : TColor;
    CorTSaida : TColor;
    procedure SetFMudaCor ( Value : TColor );
    procedure SetFVoltaCor ( Value : TColor );
    procedure SetTMudaCor ( Value : TColor );
    procedure SetTVoltaCor ( Value : TColor );
  protected
    procedure DoEnter; override;
    procedure DoExit; override;
  public

  published
    property CorFundoEntrada : TColor read CorFEntrada write SetFMudaCor;
    property CorTextoEntrada : TColor read CorTEntrada write SetTMudaCor;
    property CorFundoSaida : TColor read CorFSaida write SetFVoltaCor;
    property CorTextoSaida : TColor read CorTSaida write SetTVoltaCor;
  end;

procedure Register;

implementation


//Inicio -> Set Cor Fundo
procedure TDBMemoMC.SetFMudaCor ( Value : TColor );
begin
  CorFEntrada := Value;
end;

procedure TDBMemoMC.SetFVoltaCor ( Value : TColor );
begin
  CorFSaida := Value;
end;
//Fim -> Set Cor Texto

//Inicio -> Set Cor Texto
procedure TDBMemoMC.SetTMudaCor ( Value : TColor );
begin
  CorTEntrada := Value;
end;

procedure TDBMemoMC.SetTVoltaCor ( Value : TColor );
begin
  CorTSaida := Value;
end;
//Fim -> Set Cor Texto

procedure TDBMemoMC.DoEnter;
begin
  MaxLength := TField(Screen.ActiveForm.FindComponent(DataSource.DataSet.Name + DataField)).Size;
  Color := CorFEntrada;
  Font.Color := CorTEntrada;
  inherited DoEnter;
end;

procedure TDBMemoMC.DoExit;
begin
  Color := CorFSaida;
  Font.Color := CorTSaida;
  inherited DoExit;
end;





procedure Register;
begin
  {$I icones.lrs}
  RegisterComponents('RedID',[TDBMemoMC]);
end;

end.
