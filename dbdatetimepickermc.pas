unit DBDateTimePickerMC;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, DBDateTimePicker, StdCtrls;

type
  TDBDateTimePickerMC = class(TDBDateTimePicker)
  private
    FEdit        : TEdit;
    CorFEntrada  : TColor;
    CorFSaida    : TColor;
    CorTEntrada  : TColor;
    CorTSaida    : TColor;
    procedure SetFMudaCor(Value : TColor);
    procedure SetFVoltaCor(Value : TColor);
    procedure SetTMudaCor(Value : TColor);
    procedure SetTVoltaCor(Value : TColor);
  protected
    { Protected declarations }
    procedure DoEnter; override;
    procedure DoExit; override;
  public

  published
    property Edit            : TEdit        read FEdit        write FEdit;
    property Height                                                                default 21;
    property Width                                                                 default 141;
    property CorFundoEntrada : TColor       read CorFEntrada write SetFMudaCor;
    property CorTextoEntrada : TColor       read CorTEntrada write SetTMudaCor;
    property CorFundoSaida   : TColor       read CorFSaida   write SetFVoltaCor;
    property CorTextoSaida   : TColor       read CorTSaida   write SetTVoltaCor;
  end;

procedure Register;

implementation

//Inicio -> Set Cor Fundo
procedure TDBDateTimePickerMC.SetFMudaCor(Value : TColor);
begin
  CorFEntrada := Value;
end;

procedure TDBDateTimePickerMC.SetFVoltaCor(Value : TColor);
begin
  CorFSaida := Value;
end;
//Fim -> Set Cor Texto

//Inicio -> Set Cor Texto
procedure TDBDateTimePickerMC.SetTMudaCor(Value : TColor);
begin
  CorTEntrada := Value;
end;

procedure TDBDateTimePickerMC.SetTVoltaCor(Value : TColor);
begin
  CorTSaida := Value;
end;
//Fim -> Set Cor Texto


procedure TDBDateTimePickerMC.DoEnter;
begin
  Color := CorFEntrada;
  Font.Color := CorTEntrada;
  inherited DoEnter;
end;

procedure TDBDateTimePickerMC.DoExit;
begin
  Color := CorFSaida;
  Font.Color := CorTSaida;
  inherited DoExit;
end;

procedure Register;
begin
  {$I icones.lrs}
  RegisterComponents('RedID',[TDBDateTimePickerMC]);
end;

end.
