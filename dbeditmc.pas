unit DBEditMC;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, DbCtrls, db;

type
  TDBEditMC = class(TDBEdit)
  private
    CorFEntrada : TColor;
    CorFSaida : TColor;
    CorTEntrada : TColor;
    CorTSaida : TColor;
    procedure SetFMudaCor ( Value : TColor );
    procedure SetFVoltaCor ( Value : TColor );
    procedure SetTMudaCor ( Value : TColor );
    procedure SetTVoltaCor ( Value : TColor );
  protected
    procedure DoEnter; override;
    procedure DoExit; override;
    procedure DoClick(Sender : TObject);
  public
    constructor Create(Sender : TComponent); override;
  published
    property CorFundoEntrada : TColor read CorFEntrada write SetFMudaCor;
    property CorTextoEntrada : TColor read CorTEntrada write SetTMudaCor;
    property CorFundoSaida : TColor read CorFSaida write SetFVoltaCor;
    property CorTextoSaida : TColor read CorTSaida write SetTVoltaCor;
  end;

procedure Register;

implementation

//Inicio -> Set Cor Fundo
procedure TDBEditMC.SetFMudaCor ( Value : TColor );
begin
  CorFEntrada := Value;
end;

procedure TDBEditMC.SetFVoltaCor ( Value : TColor );
begin
  CorFSaida := Value;
end;
//Fim -> Set Cor Texto

//Inicio -> Set Cor Texto
procedure TDBEditMC.SetTMudaCor ( Value : TColor );
begin
  CorTEntrada := Value;
end;

procedure TDBEditMC.SetTVoltaCor ( Value : TColor );
begin
  CorTSaida := Value;
end;
//Fim -> Set Cor Texto

constructor TDBEditMC.Create(Sender: TComponent);
begin
  inherited Create(Sender);
  OnClick := @DoClick;
end;

procedure TDBEditMC.DoClick(Sender : TObject);
begin
  if trim(Text) <> '' then
    SelectAll;
end;

procedure TDBEditMC.DoEnter;
begin
  Color := CorFEntrada;
  Font.Color := CorTEntrada;
  inherited DoEnter;
end;

procedure TDBEditMC.DoExit;
begin
  Color := CorFSaida;
  Font.Color := CorTSaida;
  if (trim(Text) = '') and
     (DataSource.DataSet.FieldByName(DataField).DataType in [ftSmallint,
      ftInteger, ftWord, ftFloat, ftCurrency, ftBCD,
      ftLargeint, ftFMTBcd, ftBytes]) and
     (DataSource.DataSet.State in [dsEdit, dsInsert]) then
    Text := '0';
  inherited DoExit;
end;

procedure Register;
begin
  {$I icones.lrs}
  RegisterComponents('RedID',[TDBEditMC]);
end;

end.
