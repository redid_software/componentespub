{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit REDID;

{$warn 5023 off : no warning about unused units}
interface

uses
  DBEditMC, DBDateTimePickerMC, DBMemoMC, DBLookupComboBoxMC, DBEditDlgMC, 
  LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('DBEditMC', @DBEditMC.Register);
  RegisterUnit('DBDateTimePickerMC', @DBDateTimePickerMC.Register);
  RegisterUnit('DBMemoMC', @DBMemoMC.Register);
  RegisterUnit('DBLookupComboBoxMC', @DBLookupComboBoxMC.Register);
  RegisterUnit('DBEditDlgMC', @DBEditDlgMC.Register);
end;

initialization
  RegisterPackage('REDID', @Register);
end.
