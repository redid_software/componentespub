{****************************
F1DBEditButton.
Simple Edit Control with a Button
Revision history:
1.0.0: Initial release
1.0.1: Property Direct Input:Boolean: Wether user can edit the DBEdit directly or not
       Property ButtonShortCut: for keyboard savvy user.
*****************************}
unit DBEditDlgMC;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, ExtCtrls
  , DbCtrls, DB, Buttons,StdCtrls,LCLType, DBEditMC;

type

  { TF1DBEditButton }

  { TDBEditDlgMC }

  TDBEditDlgMC = class(TPanel)
  private
    { Private declarations }
    FDBEdit: TDBEditMC ;
    FButton : TBitBtn;
    FButtonClickEvent : TNotifyEvent;
    FChangeEvent : TNotifyEvent;
    FPasswordChar: Char;
    FRealMask: string;
    FButtonWidth:Integer;
    FDirectInput:Boolean;
    FButtonShortCut : TShortCut;
    function GetButtonWidth:Integer;
    function GetCharCase: TEditCharCase;
    function GetCorFEntrada : TColor;
    function GetCorFSaida : TColor;
    function GetCorTEntrada : TColor;
    function GetCorTSaida : TColor;
    function GetCustomEditMask: Boolean;
    function GetDataField: string;
    function GetDataSource: TDataSource;
    function GetField: TField;
    function GetReadOnly: Boolean;
    procedure SetButtonShortCut(AValue: TShortCut);
    procedure SetButtonWidth(AValue: Integer);
    procedure SetCharCase(AValue: TEditCharCase);
    procedure SetCorFEntrada(AValue : TColor);
    procedure SetCorFSaida(AValue : TColor);
    procedure SetCorTEntrada(AValue : TColor);
    procedure SetCorTSaida(AValue : TColor);
    procedure SetCustomEditMask(AValue: Boolean);
    procedure SetDataField(AValue: string);
    procedure SetDataSource(AValue: TDataSource);
    procedure SetDirectInput(AValue: Boolean);
    procedure SetMask(AValue: string);
    procedure SetPasswordChar(AValue: Char);
    procedure SetReadOnly(AValue: Boolean);
    function GetGlyph:TBitmap;
    procedure SetGlyph( Bitmap:TBitmap);
  protected
    { Protected declarations }
    procedure FButtonClick(Sender: TObject);
    procedure FDbEditChange(Sender:TObject);
    procedure FDbEditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure PanelSizeChange(Sender:TObject);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;

    // Exposed public property TDBEditMC
    function ExecuteAction(AAction: TBasicAction): Boolean;
    function UpdateAction(AAction: TBasicAction): Boolean;
    property Field: TField read GetField;

  published
    { Published declarations }

    // New Property
    property ButtonWidth:Integer read GetButtonWidth write SetButtonWidth default 24;
    property DirectInput:Boolean read FDirectInput write SetDirectInput;
    property ButtonShortCut : TShortCut read FButtonShortCut write SetButtonShortCut stored;

    // Exposed property TBitBtn
    property Glyph:TBitmap read GetGlyph write SetGlyph;

    // Exposed property TDBEditMC
    property CustomEditMask: Boolean read GetCustomEditMask write SetCustomEditMask default False;
    property DataField: string read GetDataField write SetDataField;
    property DataSource: TDataSource read GetDataSource write SetDataSource;
    property ReadOnly: Boolean read GetReadOnly write SetReadOnly default False;
    property Anchors;
    property AutoSize;
    property BiDiMode;
    property BorderSpacing;
    property BorderStyle;
    property CharCase: TEditCharCase read GetCharCase write SetCharCase;
    property Color;
    property Constraints;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property EditMask: string read FRealMask write SetMask;
    property Font;
    property ParentBiDiMode;
    property ParentColor;
    property ParentFont;
    property ParentShowHint;
    property PasswordChar: Char read FPasswordChar write SetPasswordChar default #0;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;

    property OnChange:TNotifyEvent read FChangeEvent write FChangeEvent;
    property OnButtonClick:TNotifyEvent read FButtonClickEvent write FButtonClickEvent;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEditingDone;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseEnter;
    property OnMouseLeave;
    property OnMouseMove;
    property OnMouseUp;
    property OnMouseWheel;
    property OnMouseWheelDown;
    property OnMouseWheelUp;
    property OnStartDrag;
    property OnUTF8KeyPress;

    //Gerenciar CORES
    property CorFundoEntrada : TColor       read GetCorFEntrada write SetCorFEntrada;
    property CorTextoEntrada : TColor       read GetCorTEntrada write SetCorTEntrada;
    property CorFundoSaida   : TColor       read GetCorFSaida   write SetCorFSaida;
    property CorTextoSaida   : TColor       read GetCorTSaida   write SetCorTSaida;

  end;

procedure Register;

implementation

function TDBEditDlgMC.GetField: TField;
begin
  Result := Self.FDBEdit.Field;
end;

function TDBEditDlgMC.GetReadOnly: Boolean;
begin
  Result := Self.FDBEdit.ReadOnly;
end;

procedure TDBEditDlgMC.SetButtonShortCut(AValue: TShortCut);
begin
  if FButtonShortCut=AValue then Exit;
  Self.FButtonShortCut:=AValue;
end;

procedure TDBEditDlgMC.SetButtonWidth(AValue: Integer);
begin
  if Self.FButtonWidth=AValue then Exit;
  Self.FDBEdit.Width:=Self.Width - AValue - 1;
  //Self.FButton.Width:=AValue;
end;

procedure TDBEditDlgMC.SetCharCase(AValue: TEditCharCase);
begin
  Self.FDBEdit.CharCase:=AValue;
end;

procedure TDBEditDlgMC.SetCorFEntrada(AValue : TColor);
begin
  Self.FDBEdit.CorFundoEntrada := AValue;
end;

procedure TDBEditDlgMC.SetCorFSaida(AValue : TColor);
begin
  Self.FDBEdit.CorFundoSaida := AValue;
end;

procedure TDBEditDlgMC.SetCorTEntrada(AValue : TColor);
begin
  Self.FDBEdit.CorTextoEntrada := AValue;
end;

procedure TDBEditDlgMC.SetCorTSaida(AValue : TColor);
begin
  Self.FDBEdit.CorTextoSaida := AValue;
end;

function TDBEditDlgMC.GetCustomEditMask: Boolean;
begin
  Result := Self.FDBEdit.CustomEditMask;
end;

function TDBEditDlgMC.GetButtonWidth: Integer;
begin
  Result := Self.FButtonWidth;
end;

function TDBEditDlgMC.GetCharCase: TEditCharCase;
begin
  Result := Self.FDBEdit.CharCase;
end;

function TDBEditDlgMC.GetCorFEntrada : TColor;
begin
  Result := Self.FDBEdit.CorFundoEntrada;
end;

function TDBEditDlgMC.GetCorFSaida : TColor;
begin
  Result := Self.FDBEdit.CorFundoSaida;
end;

function TDBEditDlgMC.GetCorTEntrada : TColor;
begin
  Result := Self.FDBEdit.CorTextoEntrada;
end;

function TDBEditDlgMC.GetCorTSaida : TColor;
begin
  Result := Self.FDBEdit.CorTextoSaida;
end;

function TDBEditDlgMC.GetDataField: string;
begin
  Result := Self.FDBEdit.DataField;
end;

function TDBEditDlgMC.GetDataSource: TDataSource;
begin
  Result := Self.FDBEdit.DataSource;
end;

procedure TDBEditDlgMC.SetCustomEditMask(AValue: Boolean);
begin
  Self.FDBEdit.CustomEditMask:=AValue;
end;

procedure TDBEditDlgMC.SetDataField(AValue: string);
begin
  Self.FDBEdit.DataField:=AValue;
end;

procedure TDBEditDlgMC.SetDataSource(AValue: TDataSource);
begin
  Self.FDBEdit.DataSource := AValue;
end;

procedure TDBEditDlgMC.SetDirectInput(AValue: Boolean);
begin
  if FDirectInput=AValue then Exit;
  FDirectInput:=AValue;
  Self.FDBEdit.ReadOnly:=not AValue;
end;

procedure TDBEditDlgMC.SetMask(AValue: string);
begin
  if FRealMask=AValue then Exit;
  FRealMask:=AValue;
end;

procedure TDBEditDlgMC.SetPasswordChar(AValue: Char);
begin
  if FPasswordChar=AValue then Exit;
  FPasswordChar:=AValue;
end;

procedure TDBEditDlgMC.SetReadOnly(AValue: Boolean);
begin
  Self.FDBEdit.ReadOnly:=AValue;
end;

procedure TDBEditDlgMC.FButtonClick(Sender: TObject);
begin
  Self.FDBEdit.SetFocus;
  if Assigned( Self.FButtonClickEvent) then begin
    Self.FButtonClickEvent(Self);
  end;
end;

procedure TDBEditDlgMC.FDbEditChange(Sender: TObject);
begin
  if Assigned( Self.FChangeEvent) then begin
    Self.FChangeEvent(Self);
  end;
  ;
end;

procedure TDBEditDlgMC.FDbEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Assigned(Self.OnKeyDown) then begin
     Self.OnKeyDown(Sender,Key,Shift);
  end;
   if LCLType.KeyToShortCut(Key,Shift) = Self.FButtonShortCut then begin
      Self.FButton.Click;
   end;
end;

procedure TDBEditDlgMC.PanelSizeChange(Sender: TObject);
begin
  Self.FDBEdit.Width:=Self.Width - Self.FButtonWidth - 1 ;
end;

function TDBEditDlgMC.GetGlyph: TBitmap;
begin
  Result := Self.FButton.Glyph;
end;

procedure TDBEditDlgMC.SetGlyph(Bitmap: TBitmap);
begin
 Self.FButton.Glyph := Bitmap;
end;


{ TDBEditDlgMC }
constructor TDBEditDlgMC.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Self.Width:=105;
  Self.FButtonWidth:=24;
  Self.FDBEdit := TDBEditMC.Create(Self);
  Self.FDBEdit.Parent := Self;
  Self.Height:=Self.FDBEdit.Height;
  Self.FDBEdit.Top:=0;
  Self.FDBEdit.Left:=0;
  Self.FDBEdit.Align:=alLeft;
  Self.FDBEdit.Anchors := Self.FDBEdit.Anchors + [akRight];
  Self.FDBEdit.ParentFont:=True;
  Self.FDBEdit.OnChange:=FChangeEvent;
  Self.FDBEdit.OnKeyDown:=@FDBEditKeyDown;
  Self.FDBEdit.Width:=Self.Width - Self.FButtonWidth - 1;

  //
  Self.FButton := TBitBtn.Create(Self);
  Self.FButton.AutoSize:=False;
  Self.FButton.Parent := Self;
  Self.FButton.Height:= Self.FDBEdit.Width;
  Self.FButton.Width:= Self.FButtonWidth;
  Self.FButton.Align:=alClient;
  Self.FButton.OnClick:=@FButtonClick;

  //
  Self.OnResize:=@Self.PanelSizeChange;
  Self.DirectInput:=True;
  Self.ButtonShortCut:=KeyToShortcut( VK_Down , [ssAlt]);
end;

function TDBEditDlgMC.ExecuteAction(AAction: TBasicAction): Boolean;
begin
  Result:= Self.FDBEdit.ExecuteAction(AAction);
end;

function TDBEditDlgMC.UpdateAction(AAction: TBasicAction): Boolean;
begin
  Result := Self.FDBEdit.UpdateAction(AAction);
end;


procedure Register;
begin
  {$I icones.lrs}
  RegisterComponents('RedID',[TDBEditDlgMC]);
end;

end.
